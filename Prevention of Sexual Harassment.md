# What is Sexual Harassment?

* <b>Quid pro quo-</b> 
Quid pro quo sexual harassment is the type of harassment that is most easily hidden. Just like other forms of sexual harassment, the perpetrator uses it to gain or maintain power over his or her victim.


* <b> Hostile-</b> 
A hostile work environment can be understood in the context of an act of sexual harassment which occurs when an employee has been accustomed to unwelcome advances, sexual innuendos, or offensive gender-related language that is sufficiently severe or pervasive.

# What kinds of behaviour cause sexual harassment?

1. <b>Some workplace conduct is clearly sexual harassment</b>

for example, unwanted kissing, touching of breasts or genitals, butt slapping, rape, other forms of sexual assault, requests for sexual favors, making sexually explicit comments, uninvited massages, sexually suggestive gestures, catcalls, ogling, or cornering someone in a tight space.

2. <b> Other Facts About Sexual Harassment</b>


* Sexist comments and actions can also be harassment.
A common misconception is that harassment must be of a sexual nature in order to be illegal. However, under Title VII, offensive conduct that is based on an employee's gender and severe or pervasive enough to create an abusive work environment is also illegal. For example, a workplace might be hostile if women are told to be more "feminine" or live up to other gender stereotypes, are left out of important meetings, and have their work sabotaged by their male coworkers.

* Sexual harassment by customers or clients. Most people are aware that sexual harassment by a manager or coworker is illegal. However, under Title VII, an employer has a responsibility to protect its employees from sexual harassment by outsiders as well. This includes customers, clients, vendors, business partners, and more. As long as the employer knows or should know that the harassment is occurring, it must take action to put a stop to it.

* Sexual harassment knows no gender. Traditionally when people think of sexual harassment, they think of a male harassing a female. While this is still the most common scenario, there have been plenty of incidents of females harassing males. Same-sex harassment—by a male against a male or a female against a female—is also illegal. The harassment does not need to be motivated by sexual desire either. It just needs to be based on the victim's gender.


# What would you do in case you face or witness any incident or repeated incidents of such behaviour?


* While each person needs to decide what action plan works best for him or herself, many individuals have found informal action facilitates the fastest resolution with the fewest complications. You can start with telling the person involved to stop the behavior. Try to be as clear as possible. For example, "It makes me uncomfortable when you rub my shoulders, please do not do this." If this does not work, you should consider putting it in writing, and tell the person what conduct you find offensive and what action you will take if it continues. For instance, "I find your sexual jokes offensive. I consider these to be sexual harassment and I will file a complaint if you continue to tell them to me." Date and sign the letter, keep a copy and have a witness watch you give it the offender.

* If none of the above works, tell your supervisor (unless he or she is the offender) or a human resource person in your organization (i.e., file a complaint). Check to see if your organization has a mediation or informal complaint resolution process. Cooperate with any investigation and document all that has happened.

* Read and understand your organization's Sexual Harassment Policy.
Understand what behavior constitutes sexual harassment.
Conduct ongoing education for your employees about what is sexual harassment and make sure that they understand the sexual harassment policy and how to report sexual harassment.
Monitor the conduct and environment of the workplace.
Encourage comments regarding the work environment, including problems regarding sexual harassment.
Let your employees know that you will not tolerate sexual harassment at the workplace and demonstrate your commitment "to zero-tolerance" by taking immediate action, when appropriate.
Post the sexual harassment policy in a prominent place and distribute the policy to all employees and suggest discussing in a staff meeting.

* Be both neutral and objective during an investigation of an incident.
During the investigation of a complaint and possible subsequent discipline of the harasser, co-workers may feel angry or threatened by the complainant and his or her supporters. Stop rumors and offensive actions by coworkers immediately if an incident occurs. It is important to demonstrate that this type of activity will not be tolerated.
If tension between coworkers is a problem, consider having a workshop on team building or communication (not, however, about a particular incident!)


