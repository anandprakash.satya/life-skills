# What is ES6?
ES6 or the ECMAScript 2015 is the 6th and major edition of the ECMAScript language specification standard. It defines the standard for the implementation of JavaScript and it has become much more popular than the previous edition ES5.

ES6 comes with significant changes to the JavaScript language. It brought several new features like, let and const keyword, rest and spread operators, template literals, classes, modules and many other enhancements to make JavaScript programming easier and more fun. In this article, we will discuss some of the best and most popular ES6 features that we can use in your everyday JavaScript coding.

![](https://www.cuelogic.com/wp-content/uploads/2021/06/Advantages-of-JavaScript-ES6-over-ES51.jpg)

* let and const Keywords
* Arrow Functions
* Multi-line Strings
* Default Parameters
* Template Literals
* Destructuring Assignment
* Enhanced Object Literals
* Promises
* Classes
* Modules

## Understanding these Features
### let and const keywords
The keyword "let" enables the users to define variables and on the other hand, "const" enables the users to define constants. Variables were previously declared using "var" which had function scope and were hoisted to the top. It means that a variable can be used before declaration. But, the "let" variables and constants have block scope which is surrounded by curly-braces "{}" and cannot be used before declaration.

let i = 10;
console.log(i);   //Output 10

const PI = 3.14;
console.log(PI);  //Output 3.14

### Arrow Functions
ES6 provides a feature known as Arrow Functions. It provides a more concise syntax for writing function expressions by removing the "function" and "return" keywords.

Arrow functions are defined using the fat arrow (=>) notation.

// Arrow function

let sumOfTwoNumbers = (a, b) => a + b;

console.log(sum(10, 20)); // Output 30

It is evident that there is no "return" or "function" keyword in the arrow function declaration.

We can also skip the parenthesis in the case when there is exactly one parameter, but will always need to use it when you have zero or more than one parameter.

But, if there are multiple expressions in the function body, then we need to wrap it with curly braces ("{}"). We also need to use the "return" statement to return the required value.

### Multi-line Strings
ES6 also provides Multi-line Strings. Users can create multi-line strings by using back-ticks(`).

It can be done as shown below :

let greeting = `Hello World,     
                Greetings to all,
                Keep Learning and Practicing!`
                
### Default Parameters
In ES6, users can provide the default values right in the signature of the functions. But, in ES5, OR operator had to be used.

//ES6

let calculateArea = function(height = 100, width = 50) {  
    // logic
}

//ES5

var calculateArea = function(height, width) {  
   height =  height || 50;
   width = width || 80;
   // logic
}

### Template Literals
ES6 introduces very simple string templates along with placeholders for the variables. The syntax for using the string template is ${PARAMETER} and is used inside of the back-ticked string.

let name = `My name is ${firstName} ${lastName}`

### Destructuring Assignment
Destructuring is one of the most popular features of ES6. The destructuring assignment is an expression that makes it easy to extract values from arrays, or properties from objects, into distinct variables.

There are two types of destructuring assignment expressions, namely, Array Destructuring and Object Destructuring. It can be used in the following manner :

//Array Destructuring

let fruits = ["Apple", "Banana"];
let [a, b] = fruits; // Array destructuring assignment

console.log(a, b);

//Object Destructuring

let person = {name: "Peter", age: 28};
let {name, age} = person; // Object destructuring assignment

console.log(name, age);

### Enhanced Object Literals
ES6 provides enhanced object literals which make it easy to quickly create objects with properties inside the curly braces.

function getMobile(manufacturer, model, year) {

   return {

      manufacturer,
      model,
      year
   }
}
getMobile("Samsung", "Galaxy", "2020");

### Promises
In ES6, Promises are used for asynchronous execution. We can use promise with the arrow function as demonstrated below.

var asyncCall =  new Promise((resolve, reject) => {

   // do something

   resolve();
}).then(()=> {   

   console.log('DON!');
})

### Classes
Previously, classes never existed in JavaScript. Classes are introduced in ES6 which looks similar to classes in other object-oriented languages, such as C++, Java, PHP, etc. But, they do not work exactly the same way. ES6 classes make it simpler to create objects, implement inheritance by using the "extends" keyword and also reuse the code efficiently.

In ES6, we can declare a class using the new "class" keyword followed by the name of the class.

class UserProfile {  

   constructor(firstName, lastName) { 

      this.firstName = firstName;

      this.lastName = lastName;     
   }  
    
   getName() {       

     console.log(`The Full-Name is ${this.firstName} ${this.lastName}`);    
   } 
}
let obj = new UserProfile('John', 'Smith');

obj.getName(); // output: The Full-Name is John Smith

### Modules
Previously, there was no native support for modules in JavaScript. ES6 introduced a new feature called modules, in which each module is represented by a separate ".js" file. We can use the "import" or "export" statement in a module to import or export variables, functions, classes or any other component from/to different files and modules.

export var num = 50; 

export function getName(fullName) {   
   //data
};
import {num, getName} from 'module';

console.log(num); // 50

## Summary
In this article, we learned about the ECMAScript 2015 or ES6, which defines the standard for JavaScript implementation.

We also learned about the top 10 features of ES6 which makes it very popular.

ES6 provides classes, modules, arrow functions, template literals, destructuring assignments and many more features which make working with JavaScript a lot easier.

# DATA TYPES
A data type is a classification of data. Programming languages need to have different datatypes to interact properly with values. You can do math with a number, but not with a sentence, so the computer classifies them differently.

1.There are six primitive (basic) datatypes:
Primitives can only hold a single value.
* strings
* numbers
* Boolean
* null
* undefined
* Symbol (new in ES6)

#### Introduction to Symbol
ES6 introduces a new primitive type called Symbol. They are helpful to implement metaprogramming in JavaScript programs.

Syntax

const mySymbol = Symbol()

const mySymbol = Symbol(stringDescription)

A symbol is just a piece of memory in which you can store some data. Each symbol will point to a different memory location. Values returned by a Symbol() constructor are unique and immutable.

* Example

Let us understand this through an example. Initially, we created two symbols without description followed by symbols with same description. In both the cases the equality operator will return false when the symbols are compared.

<script>

   const s1 = Symbol();

   const s2 = Symbol();

   console.log(typeof s1)

   console.log(s1===s2)

   const s3 = Symbol("hello");//description

   const s4 = Symbol("hello");

   console.log(s3)

   console.log(s4)

   console.log(s3==s4)

</script>

* The output of the above code will be as mentioned below −

symbol

false

Symbol(hello)

Symbol(hello)

false

2.Non-primitive datatype:
Anything that is not one of these primitives is an Object.
Objects can contain multiple values.

# DATA STRUCTURES IN ES6

![](https://d33wubrfki0l68.cloudfront.net/005ce563ff5d1f5d1f39d9c8605d57d435a7cb1a/7b5f4/assets/es6-ds@700w.0772224c.avif)

ES6 introduces two new data structures:

* Map − This data structure enables mapping a key to a value.

* Set − Sets are similar to arrays. However, sets do not encourage duplicates.

## MAP
The Map object is a simple key/value pair. Keys and values in a map may be primitive or objects.

Following is the syntax for the same.

new Map([iterable]) 
The parameter iterable represents any iterable object whose elements comprise of a key/value pair. Maps are ordered, i.e. they traverse the elements in the order of their insertion.

* Map Properties

Map.prototype.size

This property returns the number of key/value pairs in the Map object.

* Understanding basic Map operations

The set() function sets the value for the key in the Map object. 
The set() function takes two parameters namely, the key and its value. This function returns the Map object.

The has() function returns a boolean value indicating whether the specified key is found in the Map object. 
This function takes a key as parameter.

var map = new Map(); 

map.set('name','Tutorial Point'); 

map.get('name'); // Tutorial point

The above example creates a map object. The map has only one element. The element key is denoted by name. The key is mapped to a value Tutorial point.

* Note − Maps distinguish between similar values but bear different data types. In other words, an integer key 1 is considered different from a string key “1”. Consider the following example to better understand this concept

## Map Methods

1.	Map.prototype.clear()

Removes all key/value pairs from the Map object.

2.	Map.prototype.delete(key)

Removes any value associated to the key and returns the value that Map.prototype.has(key) would have previously returned.

Map.prototype.has(key) will return false afterwards.

3.	Map.prototype.entries()

Returns a new Iterator object that contains an array of [key, value] for each element in the Map object in insertion order.

4.	Map.prototype.forEach(callbackFn[, thisArg])

Calls callbackFn once for each key-value pair present in the Map object, in insertion order. If a thisArg parameter is provided to forEach, it will be used as the ‘this’ value for each callback .

5.	Map.prototype.keys()

Returns a new Iterator object that contains the keys for each element in the Map object in insertion order.

6.	Map.prototype.values()

Returns a new Iterator object that contains an array of [key, value] for each element in the Map object in insertion order.

## SAMPLE CODE
var map = new Map(); 

map.set(1,true); 

console.log(map.has("1")); //false 

map.set("1",true); 

console.log(map.has("1")); //true

* Output

false 

true 

## WeakMap
WeakMap is a small subset of map. Keys are weakly referenced, so it can be non-primitive only. If there are no reference to the object keys, it will be subject to garbage collection.

not iterable

every key is object type
The WeakMap will allow garbage collection if the key has no reference.

Syntax

The syntax for WeakMap is stated below −

new WeakMap([iterable])

* Example 1

<script>

   let emp = new WeakMap();

   emp.set(10,'Sachin');// TypeError as keys should be object

</script>

## Set
A set is an ES6 data structure. It is similar to an array with an exception that it cannot contain duplicates. In other words, it lets you store unique values. Sets support both primitive values and object references.

Just like maps, sets are also ordered, i.e. elements are iterated in their insertion order. A set can be initialized using the following syntax.

## Set Methods

1.	Set.prototype.add(value)
Appends a new element with the given value to the Set object. Returns the Set object.

2.	Set.prototype.clear()
Removes all the elements from the Set object.

3.	Set.prototype.delete(value)
Removes the element associated to the value.

4.	Set.prototype.entries()
Returns a new Iterator object that contains an array of [value, value] for each element in the Set object, in insertion order. This is kept similar to the Map object, so that each entry has the same value for its key and value here.

5.	Set.prototype.forEach(callbackFn[, thisArg])
Calls callbackFn once for each value present in the Set object, in insertion order. If athisArg parameter is provided to forEach, it will be used as the ‘this’ value for each callback.

6.	Set.prototype.has(value)
Returns a boolean asserting whether an element is present with the given value in the Set object or not.

7.	Set.prototype.values()
Returns a new Iterator object that contains the values for each element in the Set object in insertion order.

## Weak Set
Weak sets can only contain objects, and the objects they contain may be garbage collected. Like weak maps, weak sets cannot be iterated.

Example: Using a Weak Set

'use strict' 
   let weakSet = new WeakSet();  
   let obj = {msg:"hello"}; 
   weakSet.add(obj); 
   console.log(weakSet.has(obj)); 
   weakSet.delete(obj); 
   console.log(weakSet.has(obj));
The following output is displayed on successful execution of the above code.

true 

false

## SAMPLE CODE

* Example 1: Set and Iterator

var  set = new Set(['a','b','c','d','e']);  

var iterator = set.entries(); 

console.log(iterator.next())

The following output is displayed on successful execution of the above code.

{ value: [ 'a', 'a' ], done: false } 

Since, the set does not store key/value, the value array contains similar key and value. done will be false as there are more elements to be read.

* Example 2: Set and Iterator

var  set = new Set(['a','b','c','d','e']);  

var iterator = set.values(); 

console.log(iterator.next());

The following output is displayed on successful execution of the above code.

{ value: 'a', done: false }  

# RESOURCES
- [What is ES6](https://www.boardinfinity.com/blog/top-10-features-of-es6/#1-let-and-const-keywords-)
- [Javascript Data Types](https://www.tutorialspoint.com/es6/es6_symbol.htm)
- [ES6 Data Structure](https://www.tutorialspoint.com/es6/es6_collections.htm)
