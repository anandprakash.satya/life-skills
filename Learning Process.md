# Learning Process

## 1. What is the Feynman Technique?

 The Feynman Technique is a method of learning that unleashes your potential and forces you to develop a deep understanding.

 ## 2. What are the different ways to implement this technique in your learning process?

* <b>Clarify exactly what you want to learn.</b> <br>
     Clarify the concept you want to understand and write it at the top of a blank piece of paper.
     The more specific you are, the cleaner and more efficient the rest of the learning process can be.

* <b> State (and self-assess) current understanding.</b><br>
     In the plainest language possible, write down an explaination of the idea as if you were teaching it to someone who does not
     understand it at all.<br>

     Note, it’s fine to start out with a broad summary and then get more specific, working through examples, scenarios, or other
     subtleties  of the concept. Simply stating it broadly isn’t enough to fully demonstrate ‘understanding’ but rather is a kind of 
     ‘foothold’ to work from as you do demonstrate that understanding.

* <b> Acquire new knowledge.</b><br>
     In the broadest possible sense, it’s about learning–acquiring new knowledge. If you can’t explain it 
     fully, go back and reread, research, and relearn source material untill you feel more or less confident in your explaination.

     Note, if we lack expertise it can be difficult to know what we do and don’t know so this part isn’t perfect. 

* <b> Document new knowledge and clarify new understanding.</b><br>
     As you gain new knowledge, reflect on and document new knowledge–especially how your understanding has changed.

 ## 3. Paraphrase the video in detail in your own words.

  <b> The video talks about how we can unlearn the things and restructure or learning to make the learning process more efficient, which 
     are enlisted below:-</b>

*  For an example take one problem or something which you think that should be done and try to solve the problem after giving sometime 
     to the problem try to take a rest and you will see the your passive brain will think the new ways.

*   Try to play on your strengths, that is if you take more time to understand something, don't think of this as your weakness but 
     rather understand that you are learning whatever you are learning at a much deeper level.

## 4. What are some of the steps that you can take to improve your learning process?

 *  When you are learning something take a break in between, this let the brain passively think on the topic.

 *  Once Einstein said "If you can not explain something to a kid that means you do not know that very well".

 *   Try to teach the topic that you are trying to learn in simple and plain language.

 *  Identify the problem and review the resources to correct the problem.

 ## 5. Your key takeaways from the video? Paraphrase your understanding.

*    The video talks about how one should approach learning a new skill.
*    The video explains the point that one doesn't need 10,000 hours 
     to learn a some new skills it just needs 20 hours to learn enough that one can work with the skill.

 ## 6. What are some of the steps that you can while approach a new topic?

<b> Some of the steps that can be implemented while adopting a new skill are:-</b>


* learn the skill and prioritise the important topics first.


* Learn enough to self-correct your ownself, learn enough about something that you will be able to contemplate about that skill by your own.


* Remove practice barriers, that is do not judge yourself while learning a new skill if you get underconfident, it becomes harder to learn something.

* Learn something at least for 20 hours.









